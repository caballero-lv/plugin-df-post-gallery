<?php

/*
Plugin Name: DF Post Gallery
Author: Eriks Briedis
Version: 1.
Author URI: http://www.caballero.lv/
*/


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

class DF_Post_Gallery
{

    public $df_gal_dir;
    public $df_gal_url;
    public $id;
    public $ptemplate;

    function __construct()
    {

        $this->df_gal_dir = plugin_dir_path(__FILE__);
        $this->df_gal_url = plugins_url('/', __FILE__);

        if (isset($_GET['post'])) {
            $this->id = $_GET['post'];
        } else {
            if (isset($_POST['post_ID'])) {
                $this->id = $_POST['post_ID'];
            } else {
                $this->id = false;
            }
        }
        $this->ptemplate = get_post_meta($this->id, '_wp_page_template', true);

        add_action('admin_enqueue_scripts', array($this, 'add_gallery_scripts'));
        add_action('add_meta_boxes', array($this, 'add_gallery_meta_box'));
        add_action('save_post', array($this, 'save_gallery_box'));
    }

    public function add_gallery_scripts()
    {

        $screen = get_current_screen();
        if (get_post_type() != 'product') {
            return;
        }

        if ($screen->base == 'edit') {
            return;
        }

        wp_enqueue_script('jquery');
        wp_enqueue_script('df-post-gallery-js', $this->df_gal_url . 'df-post-gallery.js');
        wp_enqueue_style('df-post-gallery-css', $this->df_gal_url . 'df-post-gallery.css');
    }

    public function add_gallery_meta_box()
    {

        add_meta_box('woocommerce-product-images', __('Gallery', 'vbase'), array($this, 'render_gallery_meta_box'),
            'product', 'side');

    }

    public function render_gallery_meta_box($post)
    {
        ?>
        <div id="product_images_container">
            <ul class="product_images clearfix">
                <?php
                if (metadata_exists('post', $post->ID, '_product_image_gallery')) {
                    $product_image_gallery = get_post_meta($post->ID, '_product_image_gallery', true);
                } else {
                    // Backwards compat
                    $attachment_ids = get_posts('post_parent=' . $post->ID . '&numberposts=-1&post_type=attachment&orderby=menu_order&order=ASC&post_mime_type=image&fields=ids&meta_key=_woocommerce_exclude_image&meta_value=0');
                    $attachment_ids = array_diff($attachment_ids, array(get_post_thumbnail_id()));
                    $product_image_gallery = implode(',', $attachment_ids);
                }

                $attachments = array_filter(explode(',', $product_image_gallery));

                if ($attachments) {
                    foreach ($attachments as $attachment_id) {
                        echo '<li class="image" data-attachment_id="' . esc_attr($attachment_id) . '">
									' . wp_get_attachment_image($attachment_id, 'thumbnail') . '
									<ul class="actions">
										<li><a href="#" class="delete tips" data-tip="' . __('Delete image',
                                'woocommerce') . '">' . __('Delete', 'woocommerce') . '</a></li>
									</ul>
								</li>';
                    }
                }
                ?>
            </ul>

            <input type="hidden" id="product_image_gallery" name="product_image_gallery"
                   value="<?php echo esc_attr($product_image_gallery); ?>"/>

        </div>
        <p class="add_product_images hide-if-no-js">
            <a href="#" class="preview button" data-choose="<?php echo __('Add Images to Gallery', 'woocommerce'); ?>"
               data-update="<?php echo __('Add to gallery', 'woocommerce'); ?>"
               data-delete="<?php echo __('Delete image', 'woocommerce'); ?>"
               data-text="<?php _e('Delete', 'woocommerce'); ?>"><?php echo __('Add gallery images',
                    'woocommerce'); ?></a>
        </p>
        <?php
    }


    public function save_gallery_box($post_id)
    {


        if (isset($_POST['product_image_gallery'])) {
            $attachment_ids = array_filter(explode(',', $_POST['product_image_gallery']));
            update_post_meta($post_id, '_product_image_gallery', implode(',', $attachment_ids));

            if (!has_post_thumbnail($post_id) && isset($attachment_ids[0])) {
                set_post_thumbnail($post_id, $attachment_ids[0]);
            }
        }
    }

}

$df_post_gallery = new DF_Post_Gallery();